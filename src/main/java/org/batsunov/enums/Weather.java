package org.batsunov.enums;

public enum Weather {
    RAINY,
    CLOUDY,
    SNOWY,
    SUNNY
}
