package org.batsunov.controllers.simple;

import org.batsunov.service.PredictorImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;


@Controller
@RequestMapping("/")
@ComponentScan("org.batsunov.springConfig")
public class PredictorController {

    @Autowired
    private PredictorImpl predictor;

    @GetMapping
    public String hello(ModelMap model) {

        model.addAttribute("predicts", predictor.returnPrediction());

        return "weatherPredict";
    }
}
