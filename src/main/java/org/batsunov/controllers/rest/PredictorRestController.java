package org.batsunov.controllers.rest;

import org.batsunov.domain.ProvidedData;
import org.batsunov.exceptions.ProvidedDataNotFoundException;
import org.batsunov.repository.ProvidedDataRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;


@RestController
@RequestMapping("/api/predict")
public class PredictorRestController {

    @Autowired
    private ProvidedDataRepository providedDataRepository;

    @GetMapping
    public List getAllNotes() {
        return providedDataRepository.findAll();
    }

    // Создать запись
    @PostMapping
    public ProvidedData createNote(@Valid @RequestBody ProvidedData providedData) {

        return providedDataRepository.save(providedData);
    }


    // Получить запись по id
    @GetMapping("/{id}")
    public ProvidedData getNoteById(@PathVariable(value = "id") Long id) throws ProvidedDataNotFoundException {
        return providedDataRepository.findById(id)
                .orElseThrow(() -> new ProvidedDataNotFoundException(id));
    }

    // Обновить запись
    @PutMapping("/{id}")
    public ProvidedData updateNote(@PathVariable(value = "id") Long bookId,
                           @Valid @RequestBody ProvidedData providedDataDetails) throws ProvidedDataNotFoundException {

        ProvidedData updatedProvidedData = providedDataRepository.findById(bookId)
                .orElseThrow(() -> new ProvidedDataNotFoundException(bookId));

        updatedProvidedData.setDate(providedDataDetails.getDate());
        updatedProvidedData.setTemperature(providedDataDetails.getTemperature());
        updatedProvidedData.setWeather(providedDataDetails.getWeather());
        updatedProvidedData.setWetness(providedDataDetails.getWetness());

        return updatedProvidedData;
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Object> deleteBook(@PathVariable(value = "id") Long id) throws ProvidedDataNotFoundException {
        ProvidedData book = providedDataRepository.findById(id)
                .orElseThrow(() -> new ProvidedDataNotFoundException(id));

        providedDataRepository.delete(book);
        return ResponseEntity.ok().build();
    }
}