package org.batsunov.repository;

import org.batsunov.domain.ProvidedData;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ProvidedDataRepository extends JpaRepository<ProvidedData, Long> {

}
