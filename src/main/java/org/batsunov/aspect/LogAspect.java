package org.batsunov.aspect;

import org.aspectj.lang.JoinPoint;
import org.springframework.stereotype.Component;

@Component
public class LogAspect {
    public void beforeServiceMethodInvocation(JoinPoint jp) {
        System.out.printf("Invocation of method " + jp.getSignature());
    }

}
