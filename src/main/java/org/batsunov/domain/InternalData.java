package org.batsunov.domain;

import org.batsunov.enums.Weather;

import java.time.LocalDate;
import java.util.Objects;

/**
 * Данные которые хранятся в бд(форма)
 */

public class InternalData {

    private long id;

    private LocalDate date;
    private int temperature;
    private int wetness;
    private Weather weather;

    public InternalData(LocalDate date, int temperature, int wetness, Weather weather) {
        this.date = date;
        this.temperature = temperature;
        this.wetness = wetness;
        this.weather = weather;
    }

    public Weather getWeather() {
        return weather;
    }

    public void setWeather(Weather weather) {
        this.weather = weather;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public InternalData() {
    }

    public LocalDate getDate() {
        return date;
    }

    public void setDate(LocalDate date) {
        this.date = date;
    }

    public int getTemperature() {
        return temperature;
    }

    public void setTemperature(int temperature) {
        this.temperature = temperature;
    }

    public int getWetness() {
        return wetness;
    }

    public void setWetness(int wetness) {
        this.wetness = wetness;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        InternalData that = (InternalData) o;
        return temperature == that.temperature &&
                wetness == that.wetness &&
                Objects.equals(date, that.date) &&
                weather == that.weather;
    }

    @Override
    public int hashCode() {
        return Objects.hash(date, temperature, wetness, weather);
    }

    @Override
    public String toString() {
        return "InternalData{" +
                "date=" + date +
                ", temperature=" + temperature +
                ", wetness=" + wetness +
                ", weather=" + weather +
                '}';
    }
}
