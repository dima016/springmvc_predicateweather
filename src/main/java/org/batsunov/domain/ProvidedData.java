package org.batsunov.domain;

import org.batsunov.enums.Weather;

import javax.persistence.*;
import java.util.Date;
import java.util.Objects;

/**
 * Данные которые мы получаем из внешней системы. Требуют обработки
 */

@Entity
@Table(name = "weather")
public class ProvidedData {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;

    private Date date;
    private float temperature;
    private float wetness;
    private Weather weather;

    public ProvidedData(Date date, float temperature, float wetness, Weather weather) {
        this.date = date;
        this.temperature = temperature;
        this.wetness = wetness;
        this.weather = weather;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public Weather getWeather() {
        return weather;
    }

    public void setWeather(Weather weather) {
        this.weather = weather;
    }

    public ProvidedData() {
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public float getTemperature() {
        return temperature;
    }

    public void setTemperature(float temperature) {
        this.temperature = temperature;
    }

    public float getWetness() {
        return wetness;
    }

    public void setWetness(float wetness) {
        this.wetness = wetness;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ProvidedData that = (ProvidedData) o;
        return Float.compare(that.temperature, temperature) == 0 &&
                Float.compare(that.wetness, wetness) == 0 &&
                Objects.equals(date, that.date) &&
                weather == that.weather;
    }

    @Override
    public int hashCode() {
        return Objects.hash(date, temperature, wetness, weather);
    }

    @Override
    public String toString() {
        return "ProvidedData{" +
                "id=" + id +
                ", date=" + date +
                ", temperature=" + temperature +
                ", wetness=" + wetness +
                ", weather=" + weather +
                '}';
    }
}
