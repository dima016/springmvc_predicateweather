package org.batsunov.exceptions;

public class ProvidedDataNotFoundException extends Exception {
    public ProvidedDataNotFoundException() {
    }

    public ProvidedDataNotFoundException(String message) {
        super(message);
    }

    public ProvidedDataNotFoundException(String message, Throwable cause) {
        super(message, cause);
    }

    public ProvidedDataNotFoundException(Throwable cause) {
        super(cause);
    }

    public ProvidedDataNotFoundException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }

    public ProvidedDataNotFoundException(Long id) {
        super(String.valueOf(id));
    }
}
