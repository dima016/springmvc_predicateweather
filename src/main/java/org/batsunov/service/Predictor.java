package org.batsunov.service;

import org.batsunov.domain.InternalData;

import javax.persistence.criteria.CriteriaBuilder;
import java.util.List;

/**
 * Объект данного типа должен уметь отображать прогноз погоды на ближайшие 7 дней
 * <p>
 * Он берет данные у Storage и выводит их в консоль в удобно читаемом для пользователя формате
 */

public interface Predictor {
    List<InternalData> returnPrediction();


}
