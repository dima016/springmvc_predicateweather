package org.batsunov.service;

import org.batsunov.domain.InternalData;

import java.util.List;

/**
 * Сохранения данных и извлечение их из абстрактного хранилища
 */

public interface Storage {
    /**
     * Сохранение записи
     */

    void save(InternalData internalData);

    /**
     * Должен возвращать все доступные данные
     */

    List<InternalData> load();

    void clean();

}