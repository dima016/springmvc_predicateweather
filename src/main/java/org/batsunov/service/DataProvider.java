package org.batsunov.service;

import org.batsunov.domain.ProvidedData;

import java.util.List;

/**
 * Этот объект служит для получения данных из внешней системы
 */

public interface DataProvider {
    List<ProvidedData> get();
}
