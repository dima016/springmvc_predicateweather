package org.batsunov.service;


import org.batsunov.domain.ProvidedData;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class DataReceiverImpl implements DataReceiver {

   private DataProvider dataProvider;
   private Storage storage;
   private DataConverter converter;


    public DataProvider getDataProvider() {
        return dataProvider;
    }
    @Autowired
    public void setDataProvider(DataProvider dataProvider) {
        this.dataProvider = dataProvider;
    }

    public Storage getStorage() {
        return storage;
    }

    @Autowired
    public void setStorage(Storage storage) {
        this.storage = storage;
    }

    public DataConverter getConverter() {
        return converter;
    }

    @Autowired
    public void setConverter(DataConverter converter) {
        this.converter = converter;
    }

    @Override
    public void receive() {    //выгружает из DateProviderImpl in Storage

        storage.clean();

        for (ProvidedData el : dataProvider.get()) {
            storage.save(converter.convert(el));
        }

    }
}