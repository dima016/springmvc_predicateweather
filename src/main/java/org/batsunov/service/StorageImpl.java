package org.batsunov.service;


import org.batsunov.domain.InternalData;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class StorageImpl implements Storage {

    private List<InternalData> internalDataList = new ArrayList<>();


    @Override
    public void save(InternalData internalData) {
        internalDataList.add(internalData);
    }

    @Override
    public void clean() {
        internalDataList.removeAll(internalDataList);
    }

    @Override
    public List<InternalData> load() {
        return new ArrayList<>(internalDataList);
    }

}
