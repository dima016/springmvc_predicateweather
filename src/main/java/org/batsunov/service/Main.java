package org.batsunov.service;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.boot.autoconfigure.domain.EntityScan;

@SpringBootApplication
@ComponentScan(basePackages = "org.batsunov.*")
@EnableJpaRepositories("org.batsunov.repository")
@EntityScan("org.batsunov.domain")
public class Main {

    public static void main(String[] args) {
         SpringApplication.run(Main.class, args);
    }
}
