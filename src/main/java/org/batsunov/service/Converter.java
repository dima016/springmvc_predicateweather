package org.batsunov.service;

import org.batsunov.domain.InternalData;
import org.batsunov.domain.ProvidedData;
import org.springframework.stereotype.Component;

import java.time.ZoneId;

@Component
public class Converter implements DataConverter {


    @Override
    public InternalData convert(ProvidedData source) {
        return new InternalData(source.getDate().toInstant().atZone(ZoneId.systemDefault()).toLocalDate(),
                (int) source.getTemperature(),
                (int) source.getWetness(),
                source.getWeather());
    }
}
