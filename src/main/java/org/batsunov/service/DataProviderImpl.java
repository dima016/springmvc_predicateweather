package org.batsunov.service;

import org.batsunov.domain.ProvidedData;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.batsunov.repository.ProvidedDataRepository;

import java.util.List;

/**
 * Этот класс эмулирует реальные данные полуаемые от бд.
 * <p>
 * Не нужно тестировать
 * (Не использовать в тестах!)
 */
@Component
public class DataProviderImpl implements DataProvider {

    private ProvidedDataRepository rep;

    public ProvidedDataRepository getRep() {
        return rep;
    }

    @Autowired
    public void setRep(ProvidedDataRepository rep) {
        this.rep = rep;
    }

    @Override
    public List<ProvidedData> get() {
        return rep.findAll();
    }

}

