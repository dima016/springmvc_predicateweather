package org.batsunov.service;

import org.batsunov.domain.ProvidedData;
import org.batsunov.domain.InternalData;

/**
 * Объект служит для преобразования внешних данных во внутренние данные
 */

public interface DataConverter {
    InternalData convert(ProvidedData source);
}
