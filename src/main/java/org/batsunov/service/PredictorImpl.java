package org.batsunov.service;

import org.batsunov.domain.InternalData;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.persistence.criteria.CriteriaBuilder;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

@Component
public class PredictorImpl implements Predictor {

    private Storage storage;
    private List<InternalData> showList = new ArrayList<>();
    private DataReceiver dataReceiver;

    public PredictorImpl() {
    }

    public PredictorImpl(Storage storage, DataReceiver dataReceiver) {
        this.storage = storage;
        this.dataReceiver = dataReceiver;
    }

    public Storage getStorage() {
        return storage;
    }

    @Autowired
    public void setStorage(Storage storage) {
        this.storage = storage;
    }

    public DataReceiver getDataReceiver() {
        return dataReceiver;
    }

    @Autowired
    public void setDataReceiver(DataReceiver dataReceiver) {
        this.dataReceiver = dataReceiver;
    }

    @Override
    public List<InternalData> returnPrediction() {

        dataReceiver.receive();
        showList.removeAll(showList);
        int countPredictDay = 0;
        for (int i = 0; i < storage.load().size(); i++) {
            if (storage.load().get(i).getDate().getDayOfMonth() == LocalDate.now().getDayOfMonth()) {
                while (countPredictDay != 7 && (i + 1) <= storage.load().size()) {
                    showList.add(storage.load().get(i++));
                    countPredictDay++;
                }
                break;
            }
        }

        return new ArrayList<>(showList);
    }

}

