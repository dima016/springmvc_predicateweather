package org.batsunov.service;

import org.batsunov.domain.InternalData;
import org.batsunov.domain.ProvidedData;
import org.batsunov.enums.Weather;
import org.junit.Test;

import java.time.LocalDate;
import java.util.Calendar;
import java.util.GregorianCalendar;

import static org.junit.Assert.assertEquals;


public class ConverterTest {

    private Converter converter = new Converter();
    private ProvidedData providedData = new ProvidedData((new GregorianCalendar(2020, Calendar.OCTOBER, 16).getTime()), 14.8f, 78.8f, Weather.CLOUDY);
    private InternalData internalData;

    @Test
    public void shouldReturnCorrectInternalData() {
        internalData = new InternalData(LocalDate.of(2020, 10, 16), 14, 78, Weather.CLOUDY);

        assertEquals(internalData, converter.convert(providedData));

    }

}