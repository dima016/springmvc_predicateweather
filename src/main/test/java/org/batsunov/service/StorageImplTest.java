package org.batsunov.service;

import org.batsunov.domain.InternalData;
import org.batsunov.enums.Weather;
import org.junit.Test;

import java.time.LocalDate;

import static org.junit.Assert.assertEquals;

public class StorageImplTest {

    private StorageImpl storage = new StorageImpl();
    private InternalData internalData = new InternalData(LocalDate.of(2020,10,4),31,1, Weather.SNOWY);

    @Test
    public void shouldSaveAndReturnSameInfo() {
        storage.save(internalData);

        assertEquals(internalData,storage.load().get(0));
    }
}