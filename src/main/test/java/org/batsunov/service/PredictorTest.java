package org.batsunov.service;

import org.batsunov.domain.InternalData;
import org.batsunov.enums.Weather;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;


import java.time.LocalDate;
import java.util.List;

import static org.junit.Assert.assertEquals;


@RunWith(MockitoJUnitRunner.class)
public class PredictorTest {

    @Mock
    private Storage storage;


    private Predictor predictor;

    @Mock
    private DataReceiver dataReceiver;

    @Before
    public void setUp() throws Exception {//date=2020-10-16, temperature=14, wetness=78}
        predictor = new PredictorImpl(storage, dataReceiver);
    }

    @Test
    public void shouldCallFunction() {
        predictor.returnPrediction();

        Mockito.verify(storage).load();
    }


    @Test
    public void shouldReturnCorrectDay() {
        Mockito.when(storage.load()).thenReturn(dataList());

        assertEquals(7,predictor.returnPrediction().size());

    }

    private List<InternalData> dataList() {

        return List.of(new InternalData(LocalDate.of(2021, 2, 1), 1, 2, Weather.CLOUDY),
                new InternalData(LocalDate.of(LocalDate.now().getYear(), LocalDate.now().getMonth(), LocalDate.now().getDayOfMonth()), 15, 20, Weather.RAINY),
                new InternalData(LocalDate.of(LocalDate.now().getYear(), LocalDate.now().getMonth(), LocalDate.now().getDayOfMonth() + 1), 15, 20, Weather.RAINY),
                new InternalData(LocalDate.of(LocalDate.now().getYear(), LocalDate.now().getMonth(), LocalDate.now().getDayOfMonth() + 2), 15, 20, Weather.RAINY),
                new InternalData(LocalDate.of(LocalDate.now().getYear(), LocalDate.now().getMonth(), LocalDate.now().getDayOfMonth() + 3), 15, 20, Weather.RAINY),
                new InternalData(LocalDate.of(LocalDate.now().getYear(), LocalDate.now().getMonth(), LocalDate.now().getDayOfMonth() + 4), 15, 20, Weather.RAINY),
                new InternalData(LocalDate.of(LocalDate.now().getYear(), LocalDate.now().getMonth(), LocalDate.now().getDayOfMonth() + 5), 15, 20, Weather.RAINY),
                new InternalData(LocalDate.of(LocalDate.now().getYear(), LocalDate.now().getMonth(), LocalDate.now().getDayOfMonth() + 6), 15, 20, Weather.RAINY),
                new InternalData(LocalDate.of(LocalDate.now().getYear(), LocalDate.now().getMonth(), LocalDate.now().getDayOfMonth() + 7), 15, 20, Weather.RAINY)

        );
    }
}